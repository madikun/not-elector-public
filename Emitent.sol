
pragma ton-solidity >= 0.6.0;
pragma AbiHeader expire;

contract Emitent {

  uint64 constant MIN_STAKE = 1000000000;

  // State variable storing client information.
  mapping(address => Providers) providerslist;

  constructor() public {
    // check that contract's public key is set
    require(tvm.pubkey() != 0, 101);
    // Check that message has signature (msg.pubkey() is not zero) and message is signed with the owner's private key
    require(msg.pubkey() == tvm.pubkey(), 102);
    tvm.accept();
  }

  // Struct for storing the credit information.
  struct Providers {
    address provider;
    uint128 stakeAmount;
  }
  

  function receivePayment() public {
    if(msg.value >= MIN_STAKE) {
      _addProvider(msg.sender, msg.value);
    } else {
      _sendTransaction(msg.sender, msg.value);
    }
  }

  // Add & update providers list
  function _addProvider(address addr, uint128 amount) private {
    optional(Providers) existProvider = providerslist.fetch(addr);
    if (existProvider.hasValue()) {
    // Add prov list
      Providers p = existProvider.get();
      p.stakeAmount += amount;
      providerslist[addr] = p;
    } else {
    // Update prov list
      providerslist[addr] = Providers(addr, amount);
    }
  }
  
  // Function calls remote from RoundStarter contract
  function receiveProvidersList() external view returns (TvmCell) {
    TvmBuilder builder;
    builder.store(providerslist);
    TvmCell list = builder.toCell();
    return list;
  }

  function _sendTransaction(address addr, uint128 amount) internal pure {
		addr.transfer(amount, false, 64);
  }

  // Наказание в конце раунда, принимает сообщение от контракта со списком рейтингов провайдеров 
  // Добавить проверку контракта
  // Добавить логику штрафов
  // Добавить отправление остатков куда нибудь (система вознограждений) или оставить на газ
  function _punishment () private  {

  }
}